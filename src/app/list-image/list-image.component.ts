import { Component } from '@angular/core';
import { ImageService } from '../service/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-image',
  templateUrl: './list-image.component.html',
  styleUrl: './list-image.component.css'
})
export class ListImageComponent {
  images: any[] = [];
  searchTerm: string = '';
  filteredImages: any[] = [];
  constructor(private imageService: ImageService, private router: Router) {}

  ngOnInit() {
    this.imageService.getImages().subscribe({
      next: (data) => {
        this.images = data;
        this.filteredImages = this.images;
      },
      error: (error) => {
        console.error('Error fetching images', error);
      },
      complete: () => {
        console.log('Image fetching complete');
      },
    });
  }

  onImageClick(imageId: number) {
    this.router.navigate(['/list/details', imageId]);
  }

  filterImages() {
    if (!this.searchTerm) {
      this.filteredImages = this.images;
    } else {
      this.filteredImages = this.images.filter((image: any) =>
        image.author.toLowerCase().includes(this.searchTerm.toLowerCase())
      );
    }
  }
}
