import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ImageService } from '../../service/image.service';

@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrl: './image-detail.component.css'
})
export class ImageDetailComponent {
  image: any;

  constructor(
    private route: ActivatedRoute,
    private imageService: ImageService
  ) {}

  ngOnInit() {
    console.log('ggggggggggggggggggggggggggggg');

    const imageId = this.route.snapshot.paramMap.get('id');
    if (imageId != null) {
      this.imageService.getImageById(imageId).subscribe({
        next: (data) => {
          this.image = data;
        },
        error: (error) => {
          console.error('Error fetching image details', error);
        },
      });
    }
  }
}
