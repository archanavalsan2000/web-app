import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImageDetailRoutingModule } from './image-detail-routing.module';
import { ImageDetailComponent } from './image-detail.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    ImageDetailComponent
  ],
  imports: [
    CommonModule,
    ImageDetailRoutingModule,
    SharedModule
  ]
})
export class ImageDetailModule { }
