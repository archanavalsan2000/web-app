import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListImageRoutingModule } from './list-image-routing.module';
import { ListImageComponent } from './list-image.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ListImageComponent
  ],
  imports: [
    CommonModule,
    ListImageRoutingModule,
    FormsModule
  ]
})
export class ListImageModule { }
