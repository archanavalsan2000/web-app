import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListImageComponent } from './list-image.component';

const routes: Routes = [{path:'',component:ListImageComponent},
{ path: 'details', 
loadChildren: () => import('./image-detail/image-detail.module').then(m => m.ImageDetailModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListImageRoutingModule { }
