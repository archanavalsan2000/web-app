import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListImageComponent } from './list-image/list-image.component';

const routes: Routes = [
  { path: '', component: ListImageComponent },
  {
    path: 'list',
    loadChildren: () =>
      import('./list-image/list-image.module').then((m) => m.ListImageModule),
  },
  { path: '**', redirectTo: '/list', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
