import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private imgUrl = `${environment.IMG_URL}/v2/list?page=1&limit=30`;
  private imgDetailUrl = `${environment.IMG_URL}/id`;

  constructor(private http: HttpClient) {}

  getImages(): Observable<any> {
    return this.http.get(this.imgUrl);
  }

  getImageById(id: string): Observable<any> {
    return this.http.get(`${this.imgDetailUrl}/${id}/info`);
  }
}
